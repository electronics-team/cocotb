Source: cocotb
Section: electronics
Priority: optional
Maintainer: Debian Electronics Team <pkg-electronics-devel@lists.alioth.debian.org>
Uploaders:
 أحمد المحمودي (Ahmed El-Mahmoudy) <aelmahmoudy@users.sourceforge.net>,
Build-Depends: debhelper-compat (= 13),
               pybuild-plugin-pyproject, python3-setuptools,
               python3-all-dev, python3-find-libpython
Standards-Version: 4.7.0
Testsuite: autopkgtest-pkg-pybuild
Homepage: https://www.cocotb.org
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/electronics-team/cocotb.git
Vcs-Browser: https://salsa.debian.org/electronics-team/cocotb

Package: python3-cocotb
Architecture: any
Depends: ${misc:Depends}, ${python3:Depends}, ${shlibs:Depends},
Recommends: verilator |iverilog | ghdl
Description: library for writing VHDL and Verilog testbenches in Python
 cocotb is a coroutine based cosimulation library for writing VHDL and Verilog
 testbenches in Python.
 .
 cocotb encourages the same philosophy of design re-use and randomized
 testing as UVM, however is implemented in Python.
 .
 With cocotb, VHDL or SystemVerilog are normally only used for the design
 itself, not the testbench.
 .
 cocotb has built-in support for integrating with continuous integration
 systems, such as Jenkins, GitLab, etc. through standardized,
 machine-readable test reporting formats.
 .
 cocotb was specifically designed to lower the overhead of creating a
 test.
 .
 cocotb automatically discovers tests so that no additional step is
 required to add a test to a regression.
 .
 All verification is done using Python.
